The module does currency conversion of ubercart products. 

Dependency:
currency (http://drupal.org/project/currency)

Installation:
1. Extract the module in site's module folder
2. Enable the module
3. Set the default currency conversion in admin/settings/currency

Module overview:
The module uses the currency module to convert ubercart product prices from site's currency to
user's currency. The target currency is determined as follows
1. Administrators can force currency conversion to a target currency in admin/settings/currency by
choosing "Force ubercart target currency"
2. If administrators do not set this flag, then the target currency is determined based on user's
address (from uc_addessess module)
3. If this is not possible, then the default FROM and TO currency is determined based on value set in
admin/settings/currency

BLOCK:
A currency selection block has been provided. To use this 
1. Block create a folder sites/all/uc_currency_convert
2. Copy the flags of the nation whose currency you want to support and name it in the two digit code of 
the country e.g. US.jpg, GB.png, IN.jpg etc See below for the list of countries / currencies
3. Add this block in the block administration page
4. Give users the "choose currency" permission
Note: Once the user selects a currency manually by clicking on the flag, then this currency is retained
and the above described rules are ignored. 

Limitation:
The module supports conversion from only one source currency i.e. it does not support different currencies
per product.

Country / Currency Codes:
  AF / AFA
  AL / ALL
  DZ / DZD
  AS / USD
  AD / EUR
  AO / AOA
  AI / XCD
  AQ / NOK
  AG / XCD
  AR / ARA
  AM / AMD
  AW / AWG
  AU / AUD
  AT / EUR
  AZ / AZM
  BS / BSD
  BH / BHD
  BD / BDT
  BB / BBD
  BY / BYR
  BE / EUR
  BZ / BZD
  BJ / XAF
  BM / BMD
  BT / BTN
  BO / BOB
  BA / BAM
  BW / BWP
  BV / NOK
  BR / BRL
  IO / GBP
  BN / BND
  BG / BGL
  BF / XAF
  BI / BIF
  KH / KHR
  CM / XAF
  CA / CAD
  CV / CVE
  KY / KYD
  CF / XAF
  TD / XAF
  CL / CLF
  CN / CNY
  CX / AUD
  CC / AUD
  CO / COP
  KM / KMF
  CD / CDZ
  CG / XAF
  CK / NZD
  CR / CRC
  HR / HRK
  CU / CUP
  CY / EUR
  CZ / CZK
  DK / DKK
  DJ / DJF
  DM / XCD
  DO / DOP
  TP / TPE
  EC / USD
  EG / EGP
  SV / USD
  GQ / XAF
  ER / ERN
  EE / EEK
  ET / ETB
  ?? / EUR
  FK / FKP
  FO / DKK
  FJ / FJD
  FI / EUR
  FR / EUR
  FX / EUR
  GF / EUR
  PF / XPF
  TF / EUR
  GA / XAF
  GM / GMD
  GE / GEL
  DE / EUR
  GH / GHC
  GI / GIP
  GR / EUR
  GL / DKK
  GD / XCD
  GP / EUR
  GU / USD
  GT / GTQ
  GN / GNS
  GW / GWP
  GY / GYD
  HT / HTG
  HM / AUD
  VA / EUR
  HN / HNL
  HK / HKD
  HU / HUF
  IS / ISK
  IN / INR
  ID / IDR
  IR / IRR
  IQ / IQD
  IE / EUR
  IL / ILS
  IT / EUR
  CI / XAF
  JM / JMD
  JP / JPY
  JO / JOD
  KZ / KZT
  KE / KES
  KI / AUD
  KP / KPW
  KR / KRW
  KW / KWD
  KG / KGS
  LA / LAK
  LV / LVL
  LB / LBP
  LS / LSL
  LR / LRD
  LY / LYD
  LI / CHF
  LT / LTL
  LU / EUR
  MO / MOP
  MK / MKD
  MG / MGF
  MW / MWK
  MY / MYR
  MV / MVR
  ML / XAF
  MT / EUR
  MH / USD
  MQ / EUR
  MR / MRO
  MU / MUR
  YT / EUR
  MX / MXN
  FM / USD
  MD / MDL
  MC / EUR
  MN / MNT
  MS / XCD
  MA / MAD
  MZ / MZM
  MM / MMK
  NA / NAD
  NR / AUD
  NP / NPR
  NL / EUR
  AN / ANG
  NC / XPF
  NZ / NZD
  NI / NIC
  NE / XOF
  NG / NGN
  NU / NZD
  NF / AUD
  MP / USD
  NO / NOK
  OM / OMR
  PK / PKR
  PW / USD
  PA / PAG
  PG / PGK
  PY / PYG
  PE / PEI
  PH / PHP
  PN / NZD
  PL / PLN
  PT / EUR
  PR / USD
  QA / QAR
  RE / EUR
  RO / ROL
  RU / RUB
  RW / RWF
  KN / XCD
  LC / XCD
  VC / XCD
  WS / WST
  SM / EUR
  ST / STD
  SA / SAR
  SN / XOF
  CS / CSD
  SC / SCR
  SL / SLL
  SG / SGD
  SK / EUR
  SI / EUR
  SB / SBD
  SO / SOS
  ZA / ZAR
  GS / GBP
  ES / EUR
  LK / LKR
  SH / SHP
  PM / EUR
  SD / SDG
  SR / SRG
  SJ / NOK
  SZ / SZL
  SE / SEK
  CH / CHF
  SY / SYP
  TW / TWD
  TJ / TJR
  TZ / TZS
  TH / THB
  TG / XAF
  TK / NZD
  TO / TOP
  TT / TTD
  TN / TND
  TR / TRY
  TM / TMM
  TC / USD
  TV / AUD
  UG / UGS
  UA / UAH
  SU / SUR
  AE / AED
  GB / GBP
  US / USD
  UM / USD
  UY / UYU
  UZ / UZS
  VU / VUV
  VE / VEF
  VN / VND
  VG / USD
  VI / USD
  WF / XPF
  XO? / XOF
  EH / MAD
  YE / YER
  ZM / ZMK
  ZW / USD
